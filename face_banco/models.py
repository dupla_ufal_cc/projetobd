#coding:utf-8
from time import sleep
from django.core.exceptions import ValidationError
from django.db import models, transaction
from projetobd.teste import toDate_time

#MakeMigration = Joga as migraçoes que deverao ser feitas no proximo migrate
#migrate = efetiva as mudanças


class Perfil(models.Model):
    id_face=models.CharField(max_length=100,unique=True)
    nome=models.CharField(max_length=100,db_index=True)
    pic=models.URLField(max_length=100)
    url_perfil=models.URLField()
    username=models.URLField(blank=True,null=True)
    amigos=models.ManyToManyField('Perfil',null=True,blank=True)

    def __unicode__(self):

        return self.nome


    def pupular_perfis(self,lista):

        for perfil in lista:
            perf=Perfil()
            nome=perfil['name']
            id=perfil['id']
            pic=perfil['pic_big']
            url=perfil['url']
            username=perfil['username']
            #preenchendo um obejto do tipo perfil

            perf.nome=nome
            perf.id_face=id
            perf.pic=pic
            perf.url_perfil=url
            perf.username=username
            try:#Tratamento escapa quando o perfil ja existe
                perf.save()
            except:
                pass


def valida_folder(value):
    if value!=1 or value!=2 or value !=4:
        raise ValidationError("O valor do folder id so pode ser 0, 1 ou 4")

class ThreadMsg(models.Model):
    originator=models.ForeignKey(Perfil,db_index=True,related_name='originator')
    folder_id=models.CharField(max_length=4,validators=[valida_folder])
    recipientes=models.ManyToManyField(Perfil,related_name='recipients',null=True,blank=True)
    recent_authors=models.ManyToManyField(Perfil,related_name='recent_authors')
    subject=models.CharField(max_length=150)
    updated_time=models.DateTimeField()
    thread_id=models.CharField(max_length=70,unique=True,db_index=True)
    viewer_id=models.ForeignKey(Perfil,db_index=True)

    def __unicode__(self):

        return 'criador: %s'%self.originator.nome

    @transaction.atomic
    def face_toDatabase(self,lista):#Recebe uma lista de threads e vai salvar elas no banco de dados



        for a in lista:
            thread_id=a['thread_id']
            originator=a['originator']
            recipients=a['recipients']
            recent_authors=a['recent_authors']
            viewer_id=a['viewer_id']
            subject=a['subject']

            updated_time=toDate_time(a['updated_time'])

            folder_id=a['folder_id']

            th=ThreadMsg()

            try:
                th.originator=Perfil.objects.get(id_face=originator)#Criador da thread

                th.folder_id=folder_id
                th.subject=subject
                th.updated_time=updated_time
                th.thread_id=thread_id
                th.viewer_id=Perfil.objects.get(id_face=viewer_id)
                th.save()
                for b in recipients:

                    try:
                        p=Perfil.objects.get(id_face=b)
                        th.recipientes.add(p.id)
                        print 'recipiente inserido'
                    except Exception as e:
                        print e.message
                        print 'Algum recipiente nao cadastrado'
                for b in recent_authors:
                    try:
                        th.recent_authors.add(Perfil.objects.get(id_face=b).id)
                    except:
                        print 'Algum Autor recente nao encontrado'
                th.save()
            except:
                pass


class Message(models.Model):
    author_id=models.ForeignKey(Perfil,db_index=True)
    body=models.TextField(max_length=250)
    create_time=models.DateTimeField()
    message_id=models.CharField(max_length=200,db_index=True,unique=True)
    thread_id=models.ForeignKey(ThreadMsg)
    viewer_id=models.ForeignKey(Perfil,related_name='viewer_id')

    class Meta:
        ordering=('-create_time',)

    def __unicode__(self):

        return 'Autor: %s'%(self.author_id.nome)

    @transaction.atomic
    def face_toDatabase(self,lista):
        for a in lista:
            try:
                author_id=Perfil.objects.get(id_face=a['author_id'])
                body=a['body']
                create_time=toDate_time(a['created_time'])
                message_id=a['message_id']
                thread_id=ThreadMsg.objects.get(thread_id=a['thread_id'])
                viewer_id=Perfil.objects.get(id_face=a['viewer_id'])


                message=Message()
                message.author_id=author_id
                message.body=body
                message.create_time=create_time
                message.message_id=message_id
                message.thread_id=thread_id
                message.viewer_id=viewer_id

                message.save()

            except:
                b=1


class Token(models.Model):
    token=models.CharField(max_length=255)