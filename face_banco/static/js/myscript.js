/**
 * Created by allan on 01/03/15.
 */


window.onload=function(){
    $('#btn-amigo').click(clickAmigo);
    $('#btn-msg').click(clickMsg);
    $('#mensagens').fadeOut(1);
    $('#amigos').fadeIn(1500);


};

function clickAmigo(){
    $('#mensagens').fadeOut(1);
    $('#amigos').fadeToggle(1000);

}

function clickMsg(){
    $('#mensagens').fadeToggle(1000);
    $('#amigos').fadeOut(1);

}