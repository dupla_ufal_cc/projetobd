from django.core.exceptions import ObjectDoesNotExist

from django.core.urlresolvers import reverse
from django.db import transaction

from django.shortcuts import render, redirect
import facebook
from facebook import GraphAPIError
from face_banco.models import Perfil, ThreadMsg, Message, Token



def index(request,arg=0):
    lista=['allan','mary','eduardo','danilo']

    return render(request,'index.html',{'pessoas':False,'get_token':arg})

@transaction.atomic()
def popular_amigos(request):

    arg={}
    print 'method',request.method
    if request.method=='POST':



        try: #Exemplo de conexao com o facebook
            token=Token.objects.get(pk=1) #pk= primary key
            face=facebook.GraphAPI(access_token=token.token)#Instancia do token, caso esteja expirado lanca exessao
            fql_friends='select name,id,pic_big,url,username from profile where id=me() or id in (select uid2 from friend where uid1=me())'
            lista=face.fql(fql_friends)
            Perfil().pupular_perfis(lista) #funcao esta na model de perfil

            return redirect(reverse(viewname='listar'))

        except (GraphAPIError,ObjectDoesNotExist) as e:
            print e
            arg['get_token']=True

    return render(request,'insere_token.html',arg)
def inserir_token(request):
    return index(request,arg=True)

@transaction.atomic()
def buscar_amigos_face(request):#Popula os amigos dos amigos, na pagina do seu perfil
    arg={}

    if request.method=='POST':

        id=request.POST['id_face']

        try:
            token=Token.objects.get(pk=1)
            perf=Perfil.objects.get(id_face=id)
            face=facebook.GraphAPI(access_token=token.token)
            fql_friends='select id from profile where id in (select uid2 from friend where uid1=%s)'%perf.id_face
            lista=face.fql(fql_friends)
        except GraphAPIError:
            return perfil(request,request.POST['id_face'],True)

        for l in lista:
            id=l['id']
            try:
                ptemp=Perfil.objects.get(id_face=id)
                perf.amigos.add(ptemp.id) #Adicionando cada amigo ao perfil da pagina corrente
            except:

                pass


    return redirect(reverse(viewname='perfil',kwargs={'user_id':request.POST['id_face']}))
    #Encontra a url de perfil

def perfil(request,user_id,get_token=False):
    print get_token
    perfil=Perfil.objects.get(id_face=user_id)
    msg=Message.objects.filter(author_id__id_face=user_id)


    return render(request,'perfil.html',{'amigos':perfil.amigos.all(),'perfil':perfil,'get_token':get_token,'msg':msg})


def listar_amigos(request):
    lista=Perfil.objects.all().order_by('nome')

    lista2=[]
    cont=-1
    cont2=0
    lista2.append([])
    for l in lista:

        if cont2 % 4==0:
            lista2.append([])
            cont+=1
        lista2[cont].append(l)
        cont2+=1


    return render(request,'listarBonito.html',{'perfis':lista2})



# Views para gerenciamento das mensagens

def preencher_mensagens(request):#Crias a threads e suas respectivas mensagens
    arg={}
    if request.method=='POST':
        try:
            token=Token.objects.get(pk=1)
            face=facebook.GraphAPI(access_token=token.token)

            #Crias a Threads
            fql_thread='select thread_id,originator,recipients,recent_authors,viewer_id,subject,updated_time,folder_id from thread where folder_id=0'
            ThreadMsg().face_toDatabase(face.fql(fql_thread))

            #Cria as Mensagens
            fql_message='select author_id,body,message_id,thread_id,viewer_id,created_time from message where thread_id in(select thread_id from thread where folder_id=0)'
            Message().face_toDatabase(face.fql(fql_message))

        except (GraphAPIError,ObjectDoesNotExist):
            arg['get_token']=True
    return render(request,'popula_th_message.html',arg)


#atualiza token

def update_token(request):
    print request
    if request.method=='POST':

        token=request.POST['token']
        try:
            tk=Token.objects.get(pk=1)
            tk.token=token
            tk.save()
        except:
            Token(pk=1,token=token).save()
    return redirect(reverse(viewname='index'))


def reiniciar_base(request):

    if request.method=='POST':
        Perfil.objects.all().delete()
        return redirect(reverse(viewname='listar'))

    return render(request,'confirmar.html')