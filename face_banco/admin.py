from django.contrib import admin
from face_banco.models import Perfil, Message, Token, ThreadMsg


admin.site.register(Perfil)
admin.site.register(ThreadMsg)
admin.site.register(Message)
admin.site.register(Token)
