# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Perfil',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('id_face', models.CharField(max_length=100)),
                ('nome', models.CharField(max_length=100)),
                ('pic', models.URLField(max_length=100)),
                ('url_perfil', models.URLField()),
                ('username', models.URLField()),
                ('amigos', models.ManyToManyField(to='face_banco.Perfil')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
    ]
