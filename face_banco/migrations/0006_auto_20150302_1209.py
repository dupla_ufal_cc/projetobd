# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import face_banco.models


class Migration(migrations.Migration):

    dependencies = [
        ('face_banco', '0005_auto_20150301_2335'),
    ]

    operations = [
        migrations.CreateModel(
            name='ThreadMsg',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('folder_id', models.CharField(max_length=4, validators=[face_banco.models.valida_folder])),
                ('subject', models.CharField(max_length=150)),
                ('updated_time', models.DateTimeField()),
                ('thread_id', models.CharField(unique=True, max_length=70, db_index=True)),
                ('originator', models.ForeignKey(related_name='originator', to='face_banco.Perfil')),
                ('recent_authors', models.ManyToManyField(related_name='recent_authors', to='face_banco.Perfil')),
                ('recipientes', models.ManyToManyField(related_name='recipients', to='face_banco.Perfil')),
                ('viewer_id', models.ForeignKey(to='face_banco.Perfil')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.RemoveField(
            model_name='thread',
            name='originator',
        ),
        migrations.RemoveField(
            model_name='thread',
            name='recent_authors',
        ),
        migrations.RemoveField(
            model_name='thread',
            name='recipientes',
        ),
        migrations.RemoveField(
            model_name='thread',
            name='viewer_id',
        ),
        migrations.AlterField(
            model_name='message',
            name='thread_id',
            field=models.ForeignKey(to='face_banco.ThreadMsg'),
            preserve_default=True,
        ),
        migrations.DeleteModel(
            name='Thread',
        ),
    ]
