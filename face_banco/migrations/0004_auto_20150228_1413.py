# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import face_banco.models


class Migration(migrations.Migration):

    dependencies = [
        ('face_banco', '0003_auto_20150227_1851'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('body', models.TextField(max_length=250)),
                ('create_time', models.DateTimeField()),
                ('message_id', models.CharField(unique=True, max_length=200, db_index=True)),
                ('author_id', models.ForeignKey(to='face_banco.Perfil')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Thread',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('folder_id', models.CharField(max_length=4, validators=[face_banco.models.valida_folder])),
                ('subject', models.CharField(max_length=150)),
                ('updated_time', models.DateTimeField()),
                ('thread_id', models.CharField(unique=True, max_length=70, db_index=True)),
                ('originator', models.ForeignKey(related_name='originator', to='face_banco.Perfil')),
                ('recent_authors', models.ManyToManyField(related_name='recent_authors', to='face_banco.Perfil')),
                ('recipientes', models.ManyToManyField(related_name='recipients', to='face_banco.Perfil')),
                ('viewer_id', models.ForeignKey(to='face_banco.Perfil')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='message',
            name='thread_id',
            field=models.ForeignKey(to='face_banco.Thread'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='message',
            name='viewer_id',
            field=models.ForeignKey(related_name='viewer_id', to='face_banco.Perfil'),
            preserve_default=True,
        ),
    ]
