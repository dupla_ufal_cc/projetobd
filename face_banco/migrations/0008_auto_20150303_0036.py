# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('face_banco', '0007_auto_20150302_1309'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perfil',
            name='nome',
            field=models.CharField(max_length=100, db_index=True),
            preserve_default=True,
        ),
    ]
