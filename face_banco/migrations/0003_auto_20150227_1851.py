# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('face_banco', '0002_auto_20150227_1835'),
    ]

    operations = [
        migrations.AlterField(
            model_name='perfil',
            name='amigos',
            field=models.ManyToManyField(to='face_banco.Perfil', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AlterField(
            model_name='perfil',
            name='username',
            field=models.URLField(null=True, blank=True),
            preserve_default=True,
        ),
    ]
