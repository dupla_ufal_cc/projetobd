# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('face_banco', '0006_auto_20150302_1209'),
    ]

    operations = [
        migrations.AlterField(
            model_name='threadmsg',
            name='recipientes',
            field=models.ManyToManyField(related_name='recipients', null=True, to='face_banco.Perfil', blank=True),
            preserve_default=True,
        ),
    ]
