from django.conf.urls import patterns, include, url
from django.contrib import admin
from face_banco.views import *

urlpatterns = patterns('',
    # Examples:
    # url(r'^$', 'projetobd.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),
    url(r'^$',index,name='index'),
    url(r'popular$',popular_amigos,name='popular'),
    url(r'listar$',listar_amigos,name='listar'),
    url(r'busca/amigos$',buscar_amigos_face,name='busca_amigos'),
    url(r'perfil/(?P<user_id>\d+)/$',perfil,name='perfil'),
    url(r'preenche/message$',preencher_mensagens,name='popula_th_message'),
    url(r'update/token$',update_token,name='update_token'),
    url(r'inserir/token$',inserir_token,name='inserir_token'),
    url(r'reiniciar/base$',reiniciar_base,name='reiniciar_base'),


   )
